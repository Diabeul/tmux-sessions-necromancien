{ pkgs ? import <nixpkgs> { } }: with pkgs;
pkgs.mkShell { 
  # Get deps from the main package
  inputsFrom = [ (callPackage ./default.nix { }) ];
  # Additional tooling
  buildInputs = [
    rust-analyzer # LSP Server
    rustfmt       # Formatter
    clippy        # Linter
  ];
}
